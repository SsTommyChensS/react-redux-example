import './App.css';
import { Counter } from './features/counter/Counter';


function App() {
  // const reducer = (state = 0, action) => {
  //  switch(action.type) {
  //   case 'INCREMENT': return state + action.payload;
  //   case 'DECREMENT': return state - action.payload;
  //   default:  return state;
  //  }
  // }

  // const store = createStore(reducer);

  // store.subscribe(() => {
  //   console.log(`current state`, store.getState());
  // });

  // store.dispatch({
  //   type: 'INCREMENT',
  //   payload: 2
  // });

  // // store.dispatch({
  // //   type: 'INCREMENT',
  // //   payload: 3
  // // });

  // store.dispatch({
  //   type: 'DECREMENT',
  //   payload: 5
  // });

  return (
    <Counter/>
  )
}

export default App;

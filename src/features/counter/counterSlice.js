import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        count: 0,
    },
    reducers: {
        //Increase 1 unit
        increment: (state) => {
            state.count += 1;
        },
        //Decrease 1 unit 
        decrement: (state) => {
            state.count === 0 ? state.count = 0 : state.count -= 1;
        },
        //Increase by n units
        increaseByAmount: (state, action) => {
            state.value += action.payload;
        }
    }
})
// Action creators are generated for each case reducer function
export const { increment, decrement, increaseByAmount} = counterSlice.actions;

export const selectCount = (state) => state.counter.count;

export default counterSlice.reducer;
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment, selectCount } from "./counterSlice";

import { Button } from "react-bootstrap";

export function Counter() {
    const count = useSelector(selectCount);
    const dispatch = useDispatch();
    //Increment
    const handleIncrement = () => {
        dispatch(increment());
    };
    //Decrement
    const handleDecrement = () => {
        dispatch(decrement());
    };
    return(
        <>
            <h1>Counter Example using Redux</h1>
            <Button variant="secondary" aria-label="Increment value" onClick={handleIncrement}>Increase</Button>
            <span className="m-3">{count}</span>
            <Button variant="secondary" aria-label="Decrement value" onClick={handleDecrement}>Decrease</Button>
        </>
    )
}
